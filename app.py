from flask import Flask, render_template, redirect, url_for

application = Flask(__name__)



@application.route("/")
def index():
    return render_template("home.html", title="Form an LLC in Wyoming")

@application.route("/Wyoming-LLC-Formation")
def llc():
    return render_template("business/llc.html", title="Form an LLC in Wyoming")

@application.route("/Wyoming-Anonymous-LLC")
def anonymous():
    return render_template("anonymous/index.html", title="Anonymous Wyoming LLCs")

@application.route("/Wyoming-Holding-Company")
def holding_company():
    return render_template("holding/index.html", title="Wyoming Holding Company")

@application.route("/Wyoming-LLC-Requirements")
def requirements():
    return render_template("holding/requirements.html", title="Wyoming LLC Requirements")

@application.route("/Wyoming-LLC-Operating-Agreement")
def llc_oa():
    return render_template("holding/operating_agreement.html", title="Wyoming LLC Operating Agreement")

@application.route("/Wyoming-Sole-Proprietorship-vs-LLC")
def llc_proprietorship():
    return render_template("holding/proprietorship.html", title="LLC vs. Sole-Proprietorship")

@application.route("/Wyoming-Single-Member-LLC")
def llc_member():
    return render_template("holding/single_member.html", title="Single Member LLC")

@application.route("/Wyoming-LLC-Taxes")
def llc_taxes():
    return render_template("holding/taxes.html", title="Wyoming LLC Taxes")

@application.route("/Wyoming-Registered-Agent")
def agent():
    return render_template("agent/index.html", title="Wyoming Registered Agent")

@application.route("/Wyoming-Registered-Agent/WY-Resale-Certificate")
def agent_certificate():
    return render_template("agent/certificate.html", title="Wyoming Resellers Certificate")

@application.route("/Incorporating-in-Wyoming")
def re_llc_corporations():
    return redirect(url_for("llc_corporations"), code=301)

@application.route("/Form-a-Wyoming-Corporation")
def llc_corporations():
    return render_template("holding/corporations.html", title="Wyoming Corporations")

@application.route("/Wyoming-Trust")
def llc_trust():
    return render_template("holding/trust.html", title="Wyoming Asset Protection Trust")

@application.route("/Wyoming-LLC-Annual-Fees")
def llc_fees():
    return render_template("holding/fees.html", title="Wyoming LLC Annual Fees")

@application.route("/Wyoming-Virtual-Office")
def vo_index():
    return render_template("vo/index.html", title="Wyoming Virtual Office")

@application.route("/Wyoming-Virtual-Office/WY-Mail-Forwarding")
def vo_mail_forwarding():
    return render_template("vo/mail.html", title="Wyoming Mail Forwarding / Scanning")

@application.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

if __name__ == '__main__':
    application.run(debug=True, use_debugger=True, use_reloader=True)
