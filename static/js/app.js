// dropdown
const element = document.querySelectorAll('.dropdown-toggle')
const all_dropdown = document.querySelectorAll('.dropdown-menu')

// open dropdown (toggle)
element.forEach(function(el) {
    el.addEventListener('click', function(e){

        const parent = el.parentElement
        const dropdown = parent.querySelector('.dropdown-menu')

        // hide dropdown menu (not current)
        all_dropdown.forEach(function(menu){
            if(menu.getAttribute('aria-labelledby') !== dropdown.getAttribute('aria-labelledby')) {
                menu.classList.remove('show')
            }
        })

        // check if dropdown is open
        if(dropdown.classList.contains('show')) {
            dropdown.classList.remove('show')
        } else {
            dropdown.classList.toggle('show')
        }
    })
})

// close dropdown menu if the user click outside of it
window.onclick = function(e) {
    if(!e.target.matches('.dropdown-toggle')) {
        all_dropdown.forEach(function(menu) {
            if(menu.classList.contains('show')) {
                menu.classList.remove('show')
            }
        }) 
    }
}

// responsive burger menu open
const toggler = document.querySelector('.navbar-toggler')
toggler.addEventListener('click', function(el) {

    const parent = this.parentElement
    const icons = parent.querySelector('.material-icons')
    const burger_menu = parent.querySelector('.navbar-collapse')

    burger_menu.classList.toggle('show')

    if(burger_menu.classList.contains('show')) {
        icons.textContent = 'close'
    } else {
        icons.textContent = 'menu'
    }
})